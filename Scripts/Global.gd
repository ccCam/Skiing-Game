extends Node

#Sort the scores highest to lowest
class HighscoreSorter:
	static func sort(a, b):
		if a[1] > b[1]:
			return true
		return false

var highscores: Array = []

# Called when the node enters the scene tree for the first time.
func _ready():
	load_highscores()
	highscores.sort_custom(HighscoreSorter, "sort")
	print(highscores)
	
func load_highscores():
	#highscores = [["Cam", 20], ["Cam", 5], ["Cam", 15], ["Cam", 215], ["Cam", 10]]
	var save_game = File.new()
	if not save_game.file_exists("user://highscores.save"):
		return
	
	save_game.open("user://highscores.save", File.READ)
	var current_line = parse_json(save_game.get_line())
	print(current_line)
	highscores = current_line
	
	highscores.sort_custom(HighscoreSorter, "sort")
	print(highscores)

func save_highscores():
	highscores.sort_custom(HighscoreSorter, "sort")
	var save_game = File.new()
	save_game.open("user://highscores.save", File.WRITE)
	save_game.store_line(to_json(highscores))
	save_game.close()
	print("Saved")

