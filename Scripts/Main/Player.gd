extends KinematicBody2D


var speed_x = 0
var velocity = Vector2()

var direction = 0
var input_direction = 0

const MAX_SPEED = 40000
const ACCELERATION = 40000
const DECCELERATION = 20000

func _ready():
	set_process(true)
	
#Update
func _process(delta):

	
	if input_direction:
		direction = input_direction
	
	var move_left = Input.is_action_pressed("ui_left")
	var move_right = Input.is_action_pressed("ui_right")
	var stop_moving = !Input.is_action_pressed("ui_left") && !Input.is_action_pressed("ui_right")
	
	if move_left:
		input_direction = -1
	elif move_right:
		input_direction = 1
	elif stop_moving:
		input_direction = 0

	if input_direction == -direction:
		speed_x /= 2
	if input_direction:
		speed_x += ACCELERATION * delta
	else:
		speed_x -= DECCELERATION * delta

	
	#So speed can't go over max_speed
	#Check first isn't below second, or above first
	#Because speed is always positive and then we multiply by direction it's fine to have the lower one be 0.
	#If we were only using speed as the direction and just setting it to negative for left then we'd set the second arg as -MAX_SPEED.
	speed_x = clamp(speed_x, 0, MAX_SPEED)

	velocity.x = speed_x * delta * direction

	move_and_slide(velocity)